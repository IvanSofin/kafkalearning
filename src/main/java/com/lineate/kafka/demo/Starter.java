package com.lineate.kafka.demo;

import com.lineate.kafka.demo.configuration.KafkaProducerConfiguration;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Starter {

    public static void main(String[] args) {
        new AnnotationConfigApplicationContext(KafkaProducerConfiguration.class);
    }
}
