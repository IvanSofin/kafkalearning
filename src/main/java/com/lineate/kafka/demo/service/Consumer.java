package com.lineate.kafka.demo.service;

public interface Consumer {

    void listen(String message);
}
