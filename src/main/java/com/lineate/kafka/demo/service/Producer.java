package com.lineate.kafka.demo.service;

public interface Producer {

    void sendMessage();
}
