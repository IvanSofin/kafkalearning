package com.lineate.kafka.demo.service;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import static com.lineate.kafka.demo.Constants.TOPIC_TEST_NAME;

@Service
public class DefaultConsumer implements Consumer {

    @Override
    @KafkaListener(topics = TOPIC_TEST_NAME)
    public void listen(String message) {
        System.out.println("Message received " + message);
    }
}
