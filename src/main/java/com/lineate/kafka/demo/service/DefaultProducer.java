package com.lineate.kafka.demo.service;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;

import static com.lineate.kafka.demo.Constants.TOPIC_TEST_NAME;

@Service
@RequiredArgsConstructor
public class DefaultProducer implements Producer {

    private final KafkaTemplate kafkaTemplate;
    private int counter;

    @Scheduled(fixedDelay = 1000)
    @SneakyThrows
    @Override
    public void sendMessage() {
        System.out.println("I'll send to kafka");
        counter++;
        ListenableFuture listenableFuture = kafkaTemplate.send(TOPIC_TEST_NAME, "kafka alive, message №" + counter + " sent");
        System.out.println(listenableFuture.get());
        System.out.println("Message successfully sent to kafka");
    }
}
