package com.lineate.kafka.demo;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Constants {

    public static final String TOPIC_TEST_NAME = "ISOFIN_TEST";
    public static final String KAFKA_URL = "admarketplace-kafka-1:9092";
    public static final String CONSUMER_GROUP_ID = "foo";

}
